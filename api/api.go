package api

import (
	"encoding/json"
	"log"

	"gitlab.com/stalker-loki/srpc/interfaces"
	"gitlab.com/stalker-loki/srpc/internal/call"
)

type API struct {
	caller interfaces.Caller
}

func NewAPI(caller interfaces.Caller) *API {
	return &API{caller}
}

func (api *API) call(method string, p interface{}) (b []byte, err error) {
	var raw *json.RawMessage
	if raw, err = call.Raw(api.caller, method, p); err != nil {
		log.Print(err)
		return
	}
	if raw == nil {
		log.Print("got nil response")
		return
	}
	if b, err = raw.MarshalJSON(); err != nil {
		log.Print(err)
		return
	}
}

func (api *API) BroadcastBlock(p BroadcastBlockParams) (
	r *BroadcastBlockResult, err error) {
	var b []byte
	if b, err = api.call("broadcast_block", &p); err != nil {
		log.Print(err)
		return
	}
	r = &BroadcastBlockResult{}
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) BroadcastTransaction(p BroadcastTransactionParams) (
	r *BroadcastTransactionResult, err error) {
	var b []byte
	if b, err = api.call("broadcast_transaction", &p); err != nil {
		log.Print(err)
		return
	}
	r = &BroadcastTransactionResult{}
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) BroadcastTransactionSynchronous(
	p BroadcastTransactionSynchronousParams) (
	r *BroadcastTransactionSynchronousResult, err error) {
	var b []byte
	if b, err = api.call("broadcast_transaction_synchronous", &p); err != nil {
		log.Print(err)
		return
	}
	r = &BroadcastTransactionSynchronousResult{}
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) FindProposals(p FindProposalsParams) (r *FindProposalsResult,
	err error) {
	var b []byte
	if b, err = api.call("find_proposals", &p); err != nil {
		log.Print(err)
		return
	}
	r = &FindProposalsResult{}
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetAccountBandwidth(p GetAccountBandwidthParams) (
	r *GetAccountBandwidthResult, err error) {
	var b []byte
	if b, err = api.call("get_account_bandwidth", &p); err != nil {
		log.Print(err)
		return
	}
	r = &GetAccountBandwidthResult{}
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetAccountCount(p GetAccountCountParams) (
	r *GetAccountCountResult, err error) {
	var b []byte
	if b, err = api.call("get_account_count", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetAccountCountResult(0)
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetAccountHistory(p GetAccountHistoryParams) (
	r *GetAccountHistoryResult, err error) {
	var b []byte
	if b, err = api.call("get_account_history", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetAccountHistoryResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

//
// func (api *API) GetAccountReferences(p GetAccountReferencesParams) (r , err error) {
//
// }

func (api *API) GetAccountReputations(p GetAccountReputationsParams) (
	r *GetAccountReputationsResult, err error) {
	var b []byte
	if b, err = api.call("get_account_reputations", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetAccountReputationsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetAccountVotes(p GetAccountVotesParams) (
	r *GetAccountVotesResult, err error) {
	var b []byte
	if b, err = api.call("get_account_votes", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetAccountVotesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetAccounts(p GetAccountsParams) (r *GetAccountsResult,
	err error) {
	var b []byte
	if b, err = api.call("get_accounts", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetAccountsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetActiveVotes(p GetActiveVotesParams) (r *GetActiveVotesResult,
	err error) {
	var b []byte
	if b, err = api.call("get_active_votes", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetActiveVotesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetActiveWitnesses(p GetActiveWitnessesParams) (
	r *GetActiveWitnessesResult, err error) {
	var b []byte
	if b, err = api.call("get_active_witnesses", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetActiveWitnessesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetBlock(p GetBlockParams) (r *GetBlockResult, err error) {
	var b []byte
	if b, err = api.call("get_block", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetBlockResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetBlockHeader(p GetBlockHeaderParams) (r *GetBlockHeaderResult,
	err error) {
	var b []byte
	if b, err = api.call("get_block_header", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetBlockHeaderResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetBlog(p GetBlogParams) (r *GetBlogResult, err error) {
	var b []byte
	if b, err = api.call("get_blog", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetBlogResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetBlogAuthors(p GetBlogAuthorsParams) (r *GetBlogAuthorsResult,
	err error) {
	var b []byte
	if b, err = api.call("get_blog_authors", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetBlogAuthorsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetBlogEntries(p GetBlogEntriesParams) (r *GetBlogEntriesResult,
	err error) {
	var b []byte
	if b, err = api.call("get_blog_entries", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetBlogEntriesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetChainProperties(p GetChainPropertiesParams) (
	r *GetChainPropertiesResult, err error) {
	var b []byte
	if b, err = api.call("get_chain_properties", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetChainPropertiesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetCommentDiscussionsByPayout(
	p GetCommentDiscussionsByPayoutParams) (
	r *GetCommentDiscussionsByPayoutResult, err error) {
	var b []byte
	if b, err = api.call("get_comment_discussions_by_payout", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetCommentDiscussionsByPayoutResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetConfig(p GetConfigParams) (r *GetConfigResult, err error) {
	var b []byte
	if b, err = api.call("get_config", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetConfigResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetContent(p GetContentParams) (r *GetContentResult,
	err error) {
	var b []byte
	if b, err = api.call("get_content", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetContentResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetContentReplies(p GetContentRepliesParams) (
	r *GetContentRepliesResult, err error) {
	var b []byte
	if b, err = api.call("get_content_replies", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetContentRepliesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetConversionRequests(p GetConversionRequestsParams) (
	r *GetConversionRequestsResult, err error) {
	var b []byte
	if b, err = api.call("get_conversion_requests", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetConversionRequestsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetCurrentMedianHistoryPrice(
	p GetCurrentMedianHistoryPriceParams) (
	r *GetCurrentMedianHistoryPriceResult,
	err error) {
	var b []byte
	if b, err = api.call("get_current_median_history_price", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetCurrentMedianHistoryPriceResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByActive(p GetDiscussionsByActiveParams) (
	r *GetDiscussionsByActiveResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_active", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByActiveResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByAuthorBeforeDate(
	p GetDiscussionsByAuthorBeforeDateParams) (
	r *GetDiscussionsByAuthorBeforeDateResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_author_before_date", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByAuthorBeforeDateResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByBlog(p GetDiscussionsByBlogParams) (
	r *GetDiscussionsByBlogResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_blog", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByBlogResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByCashout(p GetDiscussionsByCashoutParams) (
	r *GetDiscussionsByCashoutResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_cashout", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByCashoutResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByChildren(p GetDiscussionsByChildrenParams) (
	r *GetDiscussionsByChildrenResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_children", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByChildrenResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByComments(p GetDiscussionsByCommentsParams) (
	r *GetDiscussionsByCommentsResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_comments", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByCommentsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByCreated(p GetDiscussionsByCreatedParams) (
	r *GetDiscussionsByCreatedResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_created", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByCreatedResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByFeed(p GetDiscussionsByFeedParams) (
	r *GetDiscussionsByFeedResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_feed", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByFeedResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByHot(p GetDiscussionsByHotParams) (
	r *GetDiscussionsByHotResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_hot", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByHotResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByPromoted(p GetDiscussionsByPromotedParams) (
	r *GetDiscussionsByPromotedResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_promoted", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByPromotedResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByTrending(p GetDiscussionsByTrendingParams) (
	r *GetDiscussionsByTrendingResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_trending", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByTrendingResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDiscussionsByVotes(p GetDiscussionsByVotesParams) (
	r *GetDiscussionsByVotesResult, err error) {
	var b []byte
	if b, err = api.call("get_discussions_by_votes", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDiscussionsByVotesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetDynamicGlobalProperties(p GetDynamicGlobalPropertiesParams) (
	r *GetDynamicGlobalPropertiesResult, err error) {
	var b []byte
	if b, err = api.call("get_dynamic_global_properties", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetDynamicGlobalPropertiesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetEscrow(p GetEscrowParams) (r *GetEscrowResult, err error) {
	var b []byte
	if b, err = api.call("get_escrow", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetEscrowResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetExpiringVestingDelegations(
	p GetExpiringVestingDelegationsParams) (
	r *GetExpiringVestingDelegationsResult, err error) {
	var b []byte
	if b, err = api.call("get_expiring_vesting_delegations", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetExpiringVestingDelegationsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetFeed(p GetFeedParams) (r *GetFeedResult, err error) {
	var b []byte
	if b, err = api.call("get_feed", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetFeedResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetFeedEntries(p GetFeedEntriesParams) (r *GetFeedEntriesResult,
	err error) {
	var b []byte
	if b, err = api.call("get_feed_entries", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetFeedEntriesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetFeedHistory(p GetFeedHistoryParams) (r *GetFeedHistoryResult,
	err error) {
	var b []byte
	if b, err = api.call("get_feed_history", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetFeedHistoryResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetFollowCount(p GetFollowCountParams) (r *GetFollowCountResult,
	err error) {
	var b []byte
	if b, err = api.call("get_follow_count", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetFollowCountResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetFollowers(p GetFollowersParams) (r *GetFollowersResult,
	err error) {
	var b []byte
	if b, err = api.call("get_followers", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetFollowersResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetFollowing(p GetFollowingParams) (r *GetFollowingResult,
	err error) {
	var b []byte
	if b, err = api.call("get_following", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetFollowingResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetHardforkVersion(p GetHardforkVersionParams) (
	r *GetHardforkVersionResult, err error) {
	var b []byte
	if b, err = api.call("get_hardfork_version", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetHardforkVersionResult(0)
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetKeyReferences(p GetKeyReferencesParams) (
	r *GetKeyReferencesResult, err error) {
	var b []byte
	if b, err = api.call("get_key_references", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetKeyReferencesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetMarketHistory(p GetMarketHistoryParams) (
	r *GetMarketHistoryResult, err error) {
	var b []byte
	if b, err = api.call("get_market_history", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetMarketHistoryResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetMarketHistoryBuckets(p GetMarketHistoryBucketsParams) (
	r *GetMarketHistoryBucketsResult, err error) {
	var b []byte
	if b, err = api.call("get_market_history_buckets", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetMarketHistoryBucketsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetNextScheduledHardfork(p GetNextScheduledHardforkParams) (
	r *GetNextScheduledHardforkResult, err error) {
	var b []byte
	if b, err = api.call("get_next_scheduled_hardfork", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetNextScheduledHardforkResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetOpenOrders(p GetOpenOrdersParams) (r *GetOpenOrdersResult,
	err error) {
	var b []byte
	if b, err = api.call("get_open_orders", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetOpenOrdersResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetOpsInBlock(p GetOpsInBlockParams) (r *GetOpsInBlockResult,
	err error) {
	var b []byte
	if b, err = api.call("get_ops_in_block", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetOpsInBlockResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetOrderBook(p GetOrderBookParams) (r *GetOrderBookResult,
	err error) {
	var b []byte
	if b, err = api.call("get_order_book", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetOrderBookResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetOwnerHistory(p GetOwnerHistoryParams) (
	r *GetOwnerHistoryResult, err error) {
	var b []byte
	if b, err = api.call("get_owner_history", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetOwnerHistoryResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetPostDiscussionsByPayout(p GetPostDiscussionsByPayoutParams) (
	r *GetPostDiscussionsByPayoutResult, err error) {
	var b []byte
	if b, err = api.call("get_post_discussions_by_payout", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetPostDiscussionsByPayoutResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetPotentialSignatures(p GetPotentialSignaturesParams) (
	r *GetPotentialSignaturesResult, err error) {
	var b []byte
	if b, err = api.call("get_potential_signatures", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetPotentialSignaturesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetRebloggedBy(p GetRebloggedByParams) (r *GetRebloggedByResult,
	err error) {
	var b []byte
	if b, err = api.call("get_reblogged_by", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetRebloggedByResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetRecentTrades(p GetRecentTradesParams) (
	r *GetRecentTradesResult, err error) {
	var b []byte
	if b, err = api.call("get_recent_trades", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetRecentTradesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetRecoveryRequest(p GetRecoveryRequestParams) (
	r *GetRecoveryRequestResult, err error) {
	var b []byte
	if b, err = api.call("get_recovery_request", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetRecoveryRequestResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetRepliesByLastUpdate(p GetRepliesByLastUpdateParams) (
	r *GetRepliesByLastUpdateResult, err error) {
	var b []byte
	if b, err = api.call("get_replies_by_last_update", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetRepliesByLastUpdateResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetRequiredSignatures(p GetRequiredSignaturesParams) (
	r *GetRequiredSignaturesResult, err error) {
	var b []byte
	if b, err = api.call("get_required_signatures", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetRequiredSignaturesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetRewardFund(p GetRewardFundParams) (r *GetRewardFundResult,
	err error) {
	var b []byte
	if b, err = api.call("get_reward_fund", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetRewardFundResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetSavingsWithdrawFrom(p GetSavingsWithdrawFromParams) (
	r *GetSavingsWithdrawFromResult, err error) {
	var b []byte
	if b, err = api.call("get_savings_withdraw_from", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetSavingsWithdrawFromResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetSavingsWithdrawTo(p GetSavingsWithdrawToParams) (
	r *GetSavingsWithdrawToResult, err error) {
	var b []byte
	if b, err = api.call("get_savings_withdraw_to", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetSavingsWithdrawToResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetState(p GetStateParams) (r *GetStateResult, err error) {
	var b []byte
	if b, err = api.call("get_state", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetStateResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetTagsUsedByAuthor(p GetTagsUsedByAuthorParams) (
	r *GetTagsUsedByAuthorResult, err error) {
	var b []byte
	if b, err = api.call("get_tags_used_by_author", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetTagsUsedByAuthorResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetTicker(p GetTickerParams) (r *GetTickerResult, err error) {
	var b []byte
	if b, err = api.call("get_ticker", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetTickerResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetTradeHistory(p GetTradeHistoryParams) (
	r *GetTradeHistoryResult, err error) {
	var b []byte
	if b, err = api.call("get_trade_history", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetTradeHistoryResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetTransaction(p GetTransactionParams) (r *GetTransactionResult,
	err error) {
	var b []byte
	if b, err = api.call("get_transaction", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetTransactionResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetTransactionHex(p GetTransactionHexParams) (
	r *GetTransactionHexResult, err error) {
	var b []byte
	if b, err = api.call("get_transaction_hex", &p); err != nil {
		log.Print(err)
		return
	}
	gts := GetTransactionHexResult("")
	r = &gts
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetTrendingTags(p GetTrendingTagsParams) (
	r *GetTrendingTagsResult, err error) {
	var b []byte
	if b, err = api.call("get_trending_tags", &p); err != nil {
		log.Print(err)
		return
	}
	r = &GetTrendingTagsResult{}
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetVersion(p GetVersionParams) (r *GetVersionResult,
	err error) {
	var b []byte
	if b, err = api.call("get_version", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetVersionResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetVestingDelegations(p GetVestingDelegationsParams) (
	r *GetVestingDelegationsResult, err error) {
	var b []byte
	if b, err = api.call("get_vesting_delegations", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetVestingDelegationsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetVolume(p GetVolumeParams) (r *GetVolumeResult, err error) {
	var b []byte
	if b, err = api.call("get_volume", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetVolumeResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetWithdrawRoutes(p GetWithdrawRoutesParams) (
	r *GetWithdrawRoutesResult, err error) {
	var b []byte
	if b, err = api.call("get_withdraw_routes", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetWithdrawRoutesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetWitnessByAccount(p GetWitnessByAccountParams) (
	r *GetWitnessByAccountResult, err error) {
	var b []byte
	if b, err = api.call("get_witness_by_account", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetWitnessByAccountResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetWitnessCount(p GetWitnessCountParams) (
	r *GetWitnessCountResult, err error) {
	var b []byte
	if b, err = api.call("get_witness_count", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetWitnessCountResult(0)
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetWitnessSchedule(p GetWitnessScheduleParams) (
	r *GetWitnessScheduleResult, err error) {
	var b []byte
	if b, err = api.call("get_witness_schedule", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetWitnessScheduleResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetWitnesses(p GetWitnessesParams) (r *GetWitnessesResult,
	err error) {
	var b []byte
	if b, err = api.call("get_witnesses", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetWitnessesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) GetWitnessesByVote(p GetWitnessesByVoteParams) (
	r *GetWitnessesByVoteResult, err error) {
	var b []byte
	if b, err = api.call("get_witnesses_by_vote", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := GetWitnessesByVoteResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) ListProposalVotes(p ListProposalVotesParams) (
	r *ListProposalVotesResult, err error) {
	var b []byte
	if b, err = api.call("list_proposal_votes", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := ListProposalVotesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) ListProposals(p ListProposalsParams) (r *ListProposalsResult,
	err error) {
	var b []byte
	if b, err = api.call("list_proposals", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := ListProposalsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) LookupAccountNames(p LookupAccountNamesParams) (
	r *LookupAccountNamesResult, err error) {
	var b []byte
	if b, err = api.call("lookup_account_names", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := LookupAccountNamesResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) LookupAccounts(p LookupAccountsParams) (r *LookupAccountsResult,
	err error) {
	var b []byte
	if b, err = api.call("lookup_accounts", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := LookupAccountsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) LookupWitnessAccounts(p LookupWitnessAccountsParams) (
	r *LookupWitnessAccountsResult, err error) {
	var b []byte
	if b, err = api.call("lookup_witness_accounts", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := LookupWitnessAccountsResult{}
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) VerifyAccountAuthority(p VerifyAccountAuthorityParams) (
	r *VerifyAccountAuthorityResult, err error) {
	var b []byte
	if b, err = api.call("verify_account_authority", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := VerifyAccountAuthorityResult(false)
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}

func (api *API) VerifyAuthority(p VerifyAuthorityParams) (
	r *VerifyAuthorityResult, err error) {
	var b []byte
	if b, err = api.call("verify_authority", &p); err != nil {
		log.Print(err)
		return
	}
	tmp := VerifyAuthorityResult(false)
	r = &tmp
	if err = json.Unmarshal(b, r); err != nil {
		log.Print(err)
	}
	return
}
