package api

import "gitlab.com/stalker-loki/srpc/types"

type BroadcastBlockResult struct{}

type BroadcastTransactionResult struct{}

type BroadcastTransactionSynchronousResult struct {
	ID       string `json:"id"`
	BlockNum int    `json:"block_num"`
	TrxNum   int    `json:"trx_num"`
	Expired  bool   `json:"expired"`
}

type FindProposalsResult []struct {
	ID         int         `json:"id"`
	ProposalID string      `json:"proposal_id"`
	Creator    string      `json:"creator"`
	Receiver   string      `json:"receiver"`
	StartDate  *types.Time `json:"start_date"`
	EndDate    *types.Time `json:"end_date"`
	DailyPay   string      `json:"daily_pay"`
	Subject    string      `json:"subject"`
	Permlink   string      `json:"permlink"`
	TotalVotes *types.Int  `json:"total_votes"`
	Status     string      `json:"status"` // "active"/"inactive"
}

type GetAccountBandwidthResult struct{} // DEPRECATED

type GetAccountCountResult int

type GetAccountHistoryResult []interface{}

// type GetAccountReferencesResult []interface{}

type GetAccountReputationsResult []interface{}

type GetAccountVotesResult []struct {
	AuthorPerm string      `json:"authorperm"`
	Weight     int         `json:"weight"`
	RShares    int         `json:"rshares"`
	Percent    float64     `json:"percent"`
	Time       *types.Time `json:"time"`
}

type Key struct {
	WeightThreshold int           `json:"weight_threshold"`
	AccountAuths    []interface{} `json:"account_auths"`
	KeyAuths        []interface{} `json:"key_auths"`
}

type GetAccountsResult []struct {
	ID                            int           `json:"id"`
	Name                          string        `json:"name"`
	Owner                         Key           `json:"owner"`
	Active                        Key           `json:"active"`
	Posting                       Key           `json:"posting"`
	MemoKey                       string        `json:"memo_key"`
	JSONMetadata                  string        `json:"json_metadata"`
	Proxy                         string        `json:"proxy"`
	LastOwnerUpdate               *types.Time   `json:"last_owner_update"`
	LastAccountUpdate             *types.Time   `json:"last_account_update"`
	Created                       *types.Time   `json:"created"`
	Mined                         bool          `json:"mined"`
	RecoveryAccount               string        `json:"recovery_account"`
	LastAccountRecovery           *types.Time   `json:"last_account_recovery"`
	ResetAccount                  *types.Time   `json:"reset_account"`
	CommentCount                  int           `json:"comment_count"`
	LifetimeVoteCount             int           `json:"lifetime_vote_count"`
	PostCount                     int           `json:"post_count"`
	CanVote                       bool          `json:"can_vote"`
	VotingPower                   int           `json:"voting_power"`
	LastVoteTime                  *types.Time   `json:"last_vote_time"`
	Balance                       *types.Steem  `json:"balance"`
	SavingsBalance                *types.Steem  `json:"savings_balance"`
	SbdBalance                    *types.SBD    `json:"sbd_balance"`
	SbdSeconds                    string        `json:"sbd_seconds"`
	SbdSecondsLastUpdate          *types.Time   `json:"sbd_seconds_last_update"`
	SbdLastInterestPayment        *types.Time   `json:"sbd_last_interest_payment"`
	SavingsSbdBalance             *types.SBD    `json:"savings_sbd_balance"`
	SavingsSbdSeconds             *types.SBD    `json:"savings_sbd_seconds"`
	SavingsSbdSecondsLastUpdate   *types.Time   `json:"savings_sbd_seconds_last_update"`
	SavingsSbdLastInterestPayment *types.Time   `json:"savings_sbd_last_interest_payment"`
	SavingsWithdrawRequests       int           `json:"savings_withdraw_requests"`
	RewardSbdBalance              *types.SBD    `json:"reward_sbd_balance"`
	RewardSteemBalance            *types.Steem  `json:"reward_steem_balance"`
	RewardVestingBalance          *types.Vests  `json:"reward_vesting_balance"`
	RewardVestingSteem            *types.Steem  `json:"reward_vesting_steem"`
	VestingShares                 *types.Vests  `json:"vesting_shares"`
	DelegatedVestingShares        *types.Vests  `json:"delegated_vesting_shares"`
	ReceivedVestingShares         *types.Vests  `json:"received_vesting_shares"`
	VestingWithdrawRate           *types.Vests  `json:"vesting_withdraw_rate"`
	NextVestingWithdrawal         *types.Time   `json:"next_vesting_withdrawal"`
	Withdrawn                     int           `json:"withdrawn"`
	ToWithdraw                    int           `json:"to_withdraw"`
	WithdrawRoutes                int           `json:"withdraw_routes"`
	CurationRewards               int           `json:"curation_rewards"`
	PostingRewards                int           `json:"posting_rewards"`
	ProxiedVsfVotes               [4]int        `json:"proxied_vsf_votes"`
	WitnessesVotedFor             int           `json:"witnesses_voted_for"`
	LastPost                      *types.Time   `json:"last_post"`
	LastRootPost                  *types.Time   `json:"last_root_post"`
	VestingBalance                *types.Steem  `json:"vesting_balance"`
	Reputation                    string        `json:"reputation"`
	TransferHistory               []interface{} `json:"transfer_history"`
	MarketHistory                 []interface{} `json:"market_history"`
	PostHistory                   []interface{} `json:"post_history"`
	VoteHistory                   []interface{} `json:"vote_history"`
	OtherHistory                  []interface{} `json:"other_history"`
	WitnessVotes                  []interface{} `json:"witness_votes"`
	TagsUsage                     []interface{} `json:"tags_usage"`
	GuestBloggers                 []interface{} `json:"guest_bloggers"`
}

type GetActiveVotesResult []struct {
	Voter      string      `json:"voter"`
	Weight     string      `json:"weight"`
	RShares    int         `json:"rshares"`
	Percent    int         `json:"percent"`
	Reputation string      `json:"reputation"`
	Time       *types.Time `json:"time"`
}

type GetActiveWitnessesResult []interface{}

type GetBlockResult struct {
	GetBlockHeaderResult
	WitnessSignature string        `json:"witness_signature"`
	Transactions     []interface{} `json:"transactions"`
	BlockID          string        `json:"block_id"`
	SigningKey       string        `json:"signing_key"`
	TransactionIDs   []interface{} `json:"transaction_ids"`
}

type GetBlockHeaderResult struct {
	Previous              string        `json:"previous"`
	Timestamp             *types.Time   `json:"timestamp"`
	Witness               string        `json:"witness"`
	TransactionMerkleRoot string        `json:"transaction_merkle_root"`
	Extensions            []interface{} `json:"extensions"`
}

type PayoutValue struct {
	Amount    int    `json:"amount"`
	Precision int    `json:"precision"`
	NAI       string `json:"nai"`
}

type Post struct {
	ID                   int         `json:"id"`
	Author               string      `json:"author"`
	PermLink             string      `json:"permlink"`
	Category             string      `json:"category"`
	ParentAuthor         string      `json:"parent_author"`
	ParentPermlink       string      `json:"parent_permlink"`
	Title                string      `json:"title"`
	Body                 string      `json:"body"`
	JSONMetadata         string      `json:"json_metadata"`
	LastUpdate           *types.Time `json:"last_update"`
	Created              *types.Time `json:"created"`
	Active               *types.Time `json:"active"`
	LastPayout           *types.Time `json:"last_payout"`
	Depth                int         `json:"depth"`
	Children             int         `json:"children"`
	NetRShares           int         `json:"net_rshares"`
	AbsRShares           int         `json:"abs_rshares"`
	VoteRShares          int         `json:"vote_rshares"`
	ChildrenAbsRShares   int         `json:"children_abs_rshares"`
	CashoutTime          *types.Time `json:"cashout_time"`
	MaxCashoutTime       *types.Time `json:"max_cashout_time"`
	TotalVoteWeight      int         `json:"total_vote_weight"`
	RewardWeight         int         `json:"reward_weight"`
	TotalPayoutValue     PayoutValue `json:"total_payout_value"`
	CuratorPayoutValue   PayoutValue `json:"curator_payout_value"`
	AuthorRewards        int         `json:"author_rewards"`
	NetVotes             int         `json:"net_votes"`
	RootAuthor           string      `json:"root_author"`
	RootPermlink         string      `json:"root_permlink"`
	MaxAcceptedPayout    PayoutValue `json:"max_accepted_payout"`
	PercentSteemDollars  int         `json:"percent_steem_dollars"`
	AllowReplies         bool        `json:"allow_replies"`
	AllowVotes           bool        `json:"allow_votes"`
	AllowCurationRewards bool        `json:"allow_curation_rewards"`
	Beneficiaries        []string    `json:"beneficiaries"`
	Blog                 string      `json:"blog"`
	ReblogOn             *types.Time `json:"reblog_on"`
	EntryID              int         `json:"entry_id"`
}

type GetBlogResult struct {
	Post
}

type GetBlogAuthorsResult []struct {
	Author string `json:"author"`
	Count  int    `json:"count"`
}

type GetBlogEntriesResult []struct {
	Author   string      `json:"author"`
	Permlink string      `json:"permlink"`
	ReblogOn *types.Time `json:"reblog_on"`
	EntryID  int         `json:"entry_id"`
}

type GetChainPropertiesResult struct {
	AccountCreationFee  *types.Steem `json:"account_creation_fee"`
	MaximumBlockSize    int          `json:"maximum_block_size"`
	SBDInterestRate     int          `json:"sbd_interest_rate"`
	AccountSubsidyLimit int          `json:"account_subsidy_limit"`
}

type GetCommentDiscussionsByPayoutResult struct{}

type GetConfigResult struct{}

type GetContentResult struct {
	ID                      int                `json:"id"`
	Author                  string             `json:"author"`
	PermLink                string             `json:"permlink"`
	Category                string             `json:"category"`
	ParentAuthor            string             `json:"parent_author"`
	ParentPermlink          string             `json:"parent_permlink"`
	Title                   string             `json:"title"`
	Body                    string             `json:"body"`
	JSONMetadata            string             `json:"json_metadata"`
	LastUpdate              *types.Time        `json:"last_update"`
	Created                 *types.Time        `json:"created"`
	Active                  *types.Time        `json:"active"`
	LastPayout              *types.Time        `json:"last_payout"`
	Depth                   int                `json:"depth"`
	Children                int                `json:"children"`
	NetRShares              int                `json:"net_rshares"`
	AbsRShares              int                `json:"abs_rshares"`
	VoteRShares             int                `json:"vote_rshares"`
	ChildrenAbsRShares      int                `json:"children_abs_rshares"`
	CashoutTime             *types.Time        `json:"cashout_time"`
	MaxCashoutTime          *types.Time        `json:"max_cashout_time"`
	TotalVoteWeight         int                `json:"total_vote_weight"`
	RewardWeight            int                `json:"reward_weight"`
	TotalPayoutValue        PayoutValue        `json:"total_payout_value"`
	CuratorPayoutValue      PayoutValue        `json:"curator_payout_value"`
	AuthorRewards           int                `json:"author_rewards"`
	NetVotes                int                `json:"net_votes"`
	RootAuthor              string             `json:"root_author"`
	RootPermlink            string             `json:"root_permlink"`
	MaxAcceptedPayout       PayoutValue        `json:"max_accepted_payout"`
	PercentSteemDollars     int                `json:"percent_steem_dollars"`
	AllowReplies            bool               `json:"allow_replies"`
	AllowVotes              bool               `json:"allow_votes"`
	AllowCurationRewards    bool               `json:"allow_curation_rewards"`
	Beneficiaries           []string           `json:"beneficiaries"`
	URL                     string             `json:"url"`
	RootTitle               string             `json:"root_title"`
	PendingPayoutValue      *types.Steem       `json:"pending_payout_value"`
	TotalPendingPayoutValue *types.Steem       `json:"total_pending_payout_value"`
	ActiveVotes             []string           `json:"active_votes"`
	Replies                 []GetContentResult `json:"replies"`
	AuthorReputation        int                `json:"author_reputation"`
	Promoted                *types.Steem       `json:"promoted"`
	BodyLength              int                `json:"body_length"`
	RebloggedBy             []string           `json:"reblogged_by"`
}

type GetContentRepliesResult GetContentResult

type GetConversionRequestsResult struct{}

type GetCurrentMedianHistoryPriceResult struct {
	Base  *types.Steem `json:"base"`
	Quote *types.Steem `json:"quote"`
}

type GetDiscussionsByActiveResult []struct {
	Tag           string   `json:"tag"`
	Limit         int      `json:"limit"`
	FilterTags    []string `json:"filter_tags"`
	SelectAuthors []string `json:"select_authors"`
	SelectTags    []string `json:"select_tags"`
	TruncateBody  int      `json:"truncate_body"`
}

type GetDiscussionsByAuthorBeforeDateResult []interface{}

type GetDiscussionsByBlogResult []interface{}

type GetDiscussionsByCashoutResult []interface{}

type GetDiscussionsByChildrenResult []interface{}

type GetDiscussionsByCommentsResult []interface{}

type GetDiscussionsByCreatedResult []interface{}

type GetDiscussionsByFeedResult []interface{}

type GetDiscussionsByHotResult []interface{}

type GetDiscussionsByPromotedResult []interface{}

type GetDiscussionsByTrendingResult []interface{}

type GetDiscussionsByVotesResult []interface{}

type GetDynamicGlobalPropertiesResult struct {
	HeadBlockNumber              int          `json:"head_block_number"`
	HeadBlockId                  string       `json:"head_block_id"`
	Time                         *types.Time  `json:"time"`
	CurrentWitness               string       `json:"current_witness"`
	TotalPow                     string       `json:"total_pow"`
	NumPowWitnesses              int          `json:"num_pow_witnesses"`
	VirtualSupply                *types.Steem `json:"virtual_supply"`
	CurrentSupply                *types.Steem `json:"current_supply"`
	ConfidentialSupply           *types.Steem `json:"confidential_supply"`
	CurrentSbdSupply             *types.Steem `json:"current_sbd_supply"`
	ConfidentialSbdSupply        *types.Steem `json:"confidential_sbd_supply"`
	TotalVestingFundSteem        *types.Steem `json:"total_vesting_fund_steem"`
	TotalVestingShares           *types.Steem `json:"total_vesting_shares"`
	TotalRewardFundSteem         *types.Steem `json:"total_reward_fund_steem"`
	TotalRewardShares2           string       `json:"total_reward_shares2"`
	PendingRewardedVestingShares *types.
					Steem `json:"pending_rewarded_vesting_shares"`
	PendingRewardedVestingSteem *types.
					Steem `json:"pending_rewarded_vesting_steem"`
	SbdInterestRate          int    `json:"sbd_interest_rate"`
	SbdPrintRate             int    `json:"sbd_print_rate"`
	MaximumBlockSize         int    `json:"maximum_block_size"`
	CurrentAslot             int    `json:"current_aslot"`
	RecentSlotsFilled        string `json:"recent_slots_filled"`
	ParticipationCount       int    `json:"participation_count"`
	LastIrreversibleBlockNum int    `json:"last_irreversible_block_num"`
	VotePowerReserveRate     int    `json:"vote_power_reserve_rate"`
}

type GetEscrowResult struct{}

type GetExpiringVestingDelegationsResult struct {
	ID            int          `json:"id"`
	Delegator     string       `json:"delegator"`
	VestingShares *types.Vests `json:"vesting_shares"`
	Expiration    *types.Time  `json:"expiration"`
}

type GetFeedResult struct{}

type GetFeedEntriesResult struct{}

type GetFeedHistoryResult struct {
	ID                   int                                `json:"id"`
	CurrentMedianHistory GetCurrentMedianHistoryPriceResult `json:"current_median_history"`
	PriceHistory         []interface{}                      `json:"price_history"`
}

type GetFollowCountResult struct {
	Account        string `json:"account"`
	FollowerCount  int    `json:"follower_count"`
	FollowingCount int    `json:"following_count"`
}

type GetFollowersResult []string

type GetFollowingResult []string

type GetHardforkVersionResult string

type GetKeyReferencesResult []string

type MarketDatapoint struct {
	High   int `json:"high"`
	Low    int `json:"low"`
	Open   int `json:"open"`
	Close  int `json:"close"`
	Volume int `json:"volume"`
}

type GetMarketHistoryResult struct {
	ID       int             `json:"id"`
	Open     *types.Time     `json:"open"`
	Seconds  int             `json:"seconds"`
	Steem    MarketDatapoint `json:"steem"`
	NonSteem MarketDatapoint `json:"non_steem"`
}

type GetMarketHistoryBucketsResult []int

type GetNextScheduledHardforkResult struct {
	HFVersion string      `json:"hf_version"`
	LiveTime  *types.Time `json:"live_time"`
}

type GetOpenOrdersResult []interface{}

type GetOpsInBlockResult struct {
	TrxID      string        `json:"trx_id"`
	Block      int           `json:"block"`
	TrxInBlock int           `json:"trx_in_block"`
	OpInTrx    int           `json:"op_in_trx"`
	VirtualOp  int           `json:"virtual_op"`
	Timestamp  *types.Time   `json:"timestamp"`
	Op         []interface{} `json:"op"`
}

type GetOrderBookResult struct {
	Bids []interface{} `json:"bids"`
	Asks []interface{} `json:"asks"`
}

type GetOwnerHistoryResult []interface{}

type GetPostDiscussionsByPayoutResult []interface{}

type GetPotentialSignaturesResult []interface{}

type GetRebloggedByResult []interface{}

type GetRecentTradesResult []struct {
	Date        *types.Time  `json:"date"`
	CurrentPays *types.SBD   `json:"current_pays"`
	OpenPays    *types.Steem `json:"open_pays"`
}

type GetRecoveryRequestResult struct{}

type GetRepliesByLastUpdateResult []interface{}

type GetRequiredSignaturesResult []interface{}

type GetRewardFundResult struct {
	ID                     int          `json:"id"`
	Name                   string       `json:"name"`
	RewardBalance          *types.Steem `json:"reward_balance"`
	RecentClaims           string       `json:"recent_claims"`
	LastUpdate             *types.Time  `json:"last_update"`
	ContentConstant        string       `json:"content_constant"`
	PercentCurationRewards int          `json:"percent_curation_rewards"`
	PercentContentRewards  int          `json:"percent_content_rewards"`
	AuthorRewardCurve      string       `json:"author_reward_curve"`
	CurationRewardCurve    string       `json:"curation_reward_curve"`
}

type GetSavingsWithdrawFromResult []interface{}

type GetSavingsWithdrawToResult []interface{}

type GetStateResult struct {
	CurrentRoute string                           `json:"current_route"`
	Props        GetDynamicGlobalPropertiesResult `json:"props"`
	TagIdx       struct {
		Trending []interface{}
	} `json:"tag_idx"`
	Tags            []string                           `json:"tags"`
	Content         []interface{}                      `json:"content"`
	Accounts        []interface{}                      `json:"accounts"`
	Witnesses       []interface{}                      `json:"witnesses"`
	DiscussionIdx   []interface{}                      `json:"discussion_idx"`
	WitnessSchedule GetWitnessScheduleResult           `json:"witness_schedule"`
	FeedPrice       GetCurrentMedianHistoryPriceResult `json:"feed_price"`
	Error           string                             `json:"error"`
}

type GetTagsUsedByAuthorResult struct {
	Tags []string `json:"tags"`
}

type GetTickerResult struct {
	Latest        string       `json:"latest"`
	LowestAsk     string       `json:"lowest_ask"`
	HighestBid    string       `json:"highest_bid"`
	PercentChange string       `json:"percent_change"`
	SteemVolume   *types.Steem `json:"steem_volume"`
	SbdVolume     *types.Steem `json:"sbd_volume"`
}

type GetTradeHistoryResult []struct {
	Date        *types.Time  `json:"date"`
	CurrentPays *types.SBD   `json:"current_pays"`
	OpenPays    *types.Steem `json:"open_pays"`
}

type GetTransactionResult struct {
	RefBlockNum    int           `json:"ref_block_num"`
	RefBlockPrefix int           `json:"ref_block_prefix"`
	Expiration     *types.Time   `json:"expiration"`
	Operations     []interface{} `json:"operations"`
	Extensions     []interface{} `json:"extensions"`
	Signatures     []interface{} `json:"signatures"`
	TransactionID  string        `json:"transaction_id"`
	BlockNum       int           `json:"block_num"`
	TransactionNum int           `json:"transaction_num"`
}

type GetTransactionHexResult string

type GetTrendingTagsResult []struct {
	Name         string     `json:"name"`
	TotalPayouts *types.SBD `json:"total_payouts"`
	NetVotes     int        `json:"net_votes"`
	TopPosts     int        `json:"top_posts"`
	Comments     int        `json:"comments"`
	Trending     string     `json:"trending"`
}

type GetVersionResult struct {
	BlockchainVersion string `json:"blockchain_version"`
	SteemRevision     string `json:"steem_revision"`
	FCRevision        string `json:"fc_revision"`
}

type GetVestingDelegationsResult []struct {
	Id                int          `json:"id"`
	Delegator         string       `json:"delegator"`
	Delegatee         string       `json:"delegatee"`
	VestingShares     *types.Vests `json:"vesting_shares"`
	MinDelegationTime *types.Time  `json:"min_delegation_time"`
}

type GetVolumeResult struct {
	SteemVolume *types.Steem `json:"steem_volume"`
	SBDVolume   *types.Steem `json:"sbd_volume"`
}

type GetWithdrawRoutesResult struct {
	ID          int    `json:"id"`
	FromAccount string `json:"from_account"`
	ToAccount   string `json:"to_account"`
	Percent     int    `json:"percent"`
	AutoVest    bool   `json:"auto_vest"`
}

type GetWitnessByAccountResult struct {
	ID                    int         `json:"id"`
	Owner                 string      `json:"owner"`
	Created               *types.Time `json:"created"`
	URL                   string      `json:"url"`
	Votes                 string      `json:"votes"`
	VirtualLastUpdate     string      `json:"virtual_last_update"`
	VirtualPosition       string      `json:"virtual_position"`
	VirtualScheduledTime  string      `json:"virtual_scheduled_time"`
	TotalMissed           int         `json:"total_missed"`
	LastAslot             int         `json:"last_aslot"`
	LastConfirmedBlockNum int         `json:"last_confirmed_block_num"`
	POWWorker             int         `json:"pow_worker"`
	SigningKey            string      `json:"signing_key"`
	Props                 struct {
		AccountCreationFee *types.Steem `json:"account_creation_fee"`
		MaximumBlockSize   int          `json:"maximum_block_size"`
		SBDInterestRate    int          `json:"sbd_interest_rate"`
	} `json:"props"`
	SBDExchangeRate       GetCurrentMedianHistoryPriceResult `json:"sbd_exchange_rate"`
	LastSBDExchangeUpdate *types.Time                        `json:"last_sbd_exchange_update"`
	LastWork              string                             `json:"last_work"`
	RunningVersion        string                             `json:"running_version"`
	HardforkVersionVote   string                             `json:"hardfork_version_vote"`
	HardforkTimeVote      *types.Time                        `json:"hardfork_time_vote"`
}

type GetWitnessCountResult int

type GetWitnessScheduleResult struct {
	ID                            int           `json:"id"`
	CurrentVirtualTime            string        `json:"current_virtual_time"`
	NextShuffleBlockNum           int           `json:"next_shuffle_block_num"`
	CurrentShuffledWitnesses      []interface{} `json:"current_shuffled_witnesses"`
	NumScheduledWitnesses         int           `json:"num_scheduled_witnesses"`
	Top19Weight                   int           `json:"top19_weight"`
	TimeshareWeight               int           `json:"timeshare_weight"`
	MinerWeight                   int           `json:"miner_weight"`
	WitnessPayNormalizationFactor int           `json:"witness_pay_normalization_factor"`
	MedianProps                   struct {
		AccountCreationFee *types.Steem `json:"account_creation_fee"`
		MaximumBlockSize   int          `json:"maximum_block_size"`
		SBDInterestRate    int          `json:"sbd_interest_rate"`
	} `json:"median_props"`
	MajorityVersion           string `json:"majority_version"`
	MaxVotedWitnesses         int    `json:"max_voted_witnesses"`
	MaxMinerWitnesses         int    `json:"max_miner_witnesses"`
	MaxRunnerWitnesses        int    `json:"max_runner_witnesses"`
	HardforkRequiredWitnesses int    `json:"hardfork_required_witnesses"`
}

type GetWitnessesResult []interface{}

type GetWitnessesByVoteResult []interface{}

type ListProposalVotesResult struct {
	ID       int    `json:"id"`
	Voter    string `json:"voter"`
	Proposal struct {
		ID         int         `json:"id"`
		ProposalID int         `json:"proposal_id"`
		Creator    string      `json:"creator"`
		Receiver   string      `json:"receiver"`
		StartDate  *types.Time `json:"start_date"`
		EndDate    *types.Time `json:"end_date"`
		DailyPay   struct {
			// "daily_pay": {
			Amount    string `json:"amount"`
			Precision int    `json:"precision"`
			NAI       string `json:"nai"`
		}
	} `json:"proposal"`
	Subject    string `json:"subject"`
	Permlink   string `json:"permlink"`
	TotalVotes string `json:"total_votes"`
	Status     string `json:"status"`
}

type ListProposalsResult struct {
	ID         int          `json:"id"`
	ProposalID int          `json:"proposal_id"`
	Creator    string       `json:"creator"`
	Receiver   string       `json:"receiver"`
	StartDate  *types.Time  `json:"start_date"`
	EndDate    *types.Time  `json:"end_date"`
	DailyPay   *types.Steem `json:"daily_pay"`
	Subject    string       `json:"subject"`
	Permlink   string       `json:"permlink"`
	TotalVotes string       `json:"total_votes"`
	Status     string       `json:"status"`
}

type LookupAccountNamesResult GetAccountsResult

type LookupAccountsResult []interface{}

type LookupWitnessAccountsResult []interface{}

type VerifyAccountAuthorityResult bool

type VerifyAuthorityResult bool
