package api

import "gitlab.com/stalker-loki/srpc/types"

type JSONRPC struct {
	Version string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
	ID      *types.Int    `json:"id"`
}

type BroadcastBlockParams []struct {
	Previous              string               `json:"previous"`
	Timestamp             *types.Time          `json:"timestamp"`
	Witness               string               `json:"witness"`
	TransactionMerkleRoot string               `json:"transaction_merkle_root"`
	Extensions            [][]interface{}      `json:"extensions"`
	WitnessSignature      string               `json:"witness_signature"`
	Transactions          []*types.Transaction `json:"transactions"`
}

type BroadcastTransactionParams []struct {
	RefBlockNum    *types.Int        `json:"ref_block_num"`
	RefBlockPrefix *types.Int        `json:"ref_block_prefix"`
	Expiration     *types.Time       `json:"expiration"`
	Operations     *types.Operations `json:"operations"`
	Extensions     [][]interface{}   `json:"extensions"`
	Signatures     []string          `json:"signatures"`
}

type BroadcastTransactionSynchronousParams BroadcastTransactionParams

type FindProposalsParams []*types.Int

type GetAccountBandwidthParams []string // account_name, type string

type GetAccountCountParams []interface{}

type GetAccountHistoryParams []interface{} // account string, start, limit int

// type GetAccountReferencesParams []interface{} // Not Implemented

type GetAccountReputationsParams []interface{} // account_lower_bound string,
// limit int

type GetAccountVotesParams string

type GetAccountsParams []string

type GetActiveVotesParams []string // author, permalink

type GetActiveWitnessesParams []interface{}

type GetBlockParams *types.Int

type GetBlockHeaderParams *types.Int

type GetBlogParams []interface{} // account string, start_entry_id, limit int

type GetBlogAuthorsParams struct {
	BlogAccount string `json:"blog_account"`
}

type GetBlogEntriesParams []interface{} // account string, start_entry_id, limit int

type GetChainPropertiesParams []interface{}

type GetDiscussionPrototype []struct {
	Tag           string     `json:"tag"`
	Limit         *types.Int `json:"limit"`
	FilterTags    []string   `json:"filter_tags"`
	SelectAuthors []string   `json:"select_authors"`
	SelectTags    []string   `json:"select_tags"`
	TruncateBody  *types.Int `json:"truncate_body"`
}

type GetCommentDiscussionsByPayoutParams GetDiscussionPrototype

type GetConfigParams []interface{}

type GetContentParams []string // author, permalink

type GetContentRepliesParams []string // author, permalink

type GetConversionRequestsParams *types.Int

type GetCurrentMedianHistoryPriceParams []interface{}

type GetDiscussionsByActiveParams []struct {
	Tag          string     `json:"tag"`
	Limit        *types.Int `json:"limit"`
	FilterTags   []string   `json:"filter_tags"`
	SelectTags   []string   `json:"select_tags"`
	TruncateBody *types.Int `json:"truncate_body"`
}

type GetDiscussionsByAuthorBeforeDateParams []interface{} // author,
// start_permalink string, before_date time, limit int

type GetDiscussionsByBlogParams GetDiscussionsByActiveParams

type GetDiscussionsByCashoutParams GetDiscussionsByActiveParams

type GetDiscussionsByChildrenParams GetDiscussionsByActiveParams

type GetDiscussionsByCommentsParams []struct {
	StartAuthor    string     `json:"start_author"`
	StartPermalink string     `json:"start_permalink"`
	Limit          *types.Int `json:"limit"`
}
type GetDiscussionsByCreatedParams GetDiscussionPrototype

type GetDiscussionsByFeedParams struct {
	Tag string `json:"tag"`
	GetDiscussionsByCommentsParams
}

type GetDiscussionsByHotParams GetDiscussionPrototype

type GetDiscussionsByPromotedParams GetDiscussionPrototype

type GetDiscussionsByTrendingParams GetDiscussionPrototype

type GetDiscussionsByVotesParams GetDiscussionPrototype

type GetDynamicGlobalPropertiesParams []interface{}

type GetEscrowParams []interface{} // start []interface{ ["alice", 99]/true,
// "1960-01-01T00:00:00" }, limit int,// order string "by_from_id"/"by_ratification_deadline"

type GetExpiringVestingDelegationsParams []interface{} // account string,
// after time

type GetFeedParams []interface{} // account string, start_entry_id, limit int

type GetFeedEntriesParams []interface{} // account string, start_entry_id, limit int

type GetFeedHistoryParams []interface{}

type GetFollowCountParams []string

type GetFollowersParams []interface{} // account, start, type string, limit int

type GetFollowingParams GetFollowersParams

type GetHardforkVersionParams []interface{}

type GetKeyReferencesParams [][]string // steem base58 key

type GetMarketHistoryParams []interface{} // bucket_seconds int,
// start, end time

type GetMarketHistoryBucketsParams []interface{}

type GetNextScheduledHardforkParams []interface{}

type GetOpenOrdersParams string // account

type GetOpsInBlockParams []interface{} // block_num int, only_virtual bool

type GetOrderBookParams []int // limit

type GetOwnerHistoryParams []string // account

type GetPostDiscussionsByPayoutParams GetDiscussionPrototype

type GetPotentialSignaturesParams BroadcastTransactionParams

type GetRebloggedByParams []interface{} // author, permalink string

type GetRecentTradesParams []int // limit

type GetRecoveryRequestParams []string // account

type GetRepliesByLastUpdateParams []interface{} // start_parent_author,
// start_permalink string, limit int

type GetRequiredSignaturesParams []interface{} // BroadcastTransactionParams,
// keys []string

type GetRewardFundParams []string // permalink

type GetSavingsWithdrawFromParams []string // account

type GetSavingsWithdrawToParams []string // account

type GetStateParams []string // account DEPRECATED

type GetTagsUsedByAuthorParams []string // account

type GetTickerParams []interface{}

type GetTradeHistoryParams []interface{} // start, end time, limit int

type GetTransactionParams []string // trx_id

type GetTransactionHexParams []BroadcastTransactionParams

type GetTrendingTagsParams struct {
	AfterTag string
	Limit    uint32
}

type GetVersionParams []interface{}

type GetVestingDelegationsParams []interface{} // delegator_account,
// start_account string, limit int <=1000

type GetVolumeParams []interface{}

type GetWithdrawRoutesParams []interface{} // account, type string

type GetWitnessByAccountParams []string // account

type GetWitnessCountParams []interface{}

type GetWitnessScheduleParams []interface{}

type GetWitnessesParams [][]int

type GetWitnessesByVoteParams []interface{} // account string, limit int

type ListProposalVotesParams []interface{} // start []interface{} (
// account/proposal_id), limit int,
// order "by_voter_proposal"/"by_proposal_voter",
// order_direction "ascending"/"descending", status "all","inactive",
// "active", "expired", "votable"

type ListProposalsParams []interface{} // start []interface{} (
// creator/start_date/end_date/total_votes), limit int,
// order "by_voter_proposal"/"by_proposal_voter",
// order_direction "ascending"/"descending", status "all","inactive",
// "active", "expired", "votable"

type LookupAccountNamesParams [][]string // accounts

type LookupAccountsParams []interface{} // lower_bound_name string, limit int

type LookupWitnessAccountsParams []interface{} // lower_bound_name string, limit int

type VerifyAccountAuthorityParams int // Not Implemented

type VerifyAuthorityParams []BroadcastTransactionParams
