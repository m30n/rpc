module gitlab.com/stalker-loki/srpc

go 1.14

require (
	github.com/btcsuite/btcutil v1.0.2
	github.com/gorilla/websocket v1.4.2
	github.com/pkg/errors v0.9.1
	github.com/sourcegraph/jsonrpc2 v0.0.0-20200429184054-15c2290dcb37
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
	gopkg.in/tomb.v2 v2.0.0-20161208151619-d5d1b5820637
)
