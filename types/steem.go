package types

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/stalker-loki/srpc/encoding/transaction"
)

type Steem struct {
	Symbol string
	n      float64
}

func NewSteem(value float64) *Steem {
	return &Steem{Symbol: "STEEM", n: value}
}

func (t *Steem) MarshalJSON() ([]byte, error) {
	return []byte(
		fmt.Sprintf("\"%f.8 "+"STEEM"+"\"", t.n)), nil
}

func (t *Steem) UnmarshalJSON(data []byte) error {
	split := strings.Split(string(data), " ")
	if len(split) != 2 {
		return errors.New("currency value not correctly formatted")
	}
	if f, err := strconv.ParseFloat(split[0], 64); err != nil {
		return errors.New("'" + split[0] +
			"' is not a valid floating point number")
	} else {
		t.Symbol = split[1]
		t.n = f
		return nil
	}
}

func (t *Steem) MarshalTransaction(encoder *transaction.Encoder) error {
	return encoder.Encode(t.n)
}
