package types

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/stalker-loki/srpc/encoding/transaction"
)

type SBD struct {
	Symbol string
	n      float64
}

func NewSBD(value float64) *SBD {
	return &SBD{Symbol: "SBD", n: value}
}

func (t *SBD) MarshalJSON() ([]byte, error) {
	return []byte(
		fmt.Sprintf("\"%f.8 "+"SBD"+"\"", t.n)), nil
}

func (t *SBD) UnmarshalJSON(data []byte) error {
	split := strings.Split(string(data), " ")
	if len(split) != 2 {
		return errors.New("currency value not correctly formatted")
	}
	if f, err := strconv.ParseFloat(split[0], 64); err != nil {
		return errors.New("'" + split[0] +
			"' is not a valid floating point number")
	} else {
		t.Symbol = split[1]
		t.n = f
		return nil
	}
}

func (t *SBD) MarshalTransaction(encoder *transaction.Encoder) error {
	return encoder.Encode(t.n)
}
